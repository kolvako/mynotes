# Exemples de syntaxe Markdown
## Markdown est un langage de balisage léger,avec une syntaxe facile à lire et à écrire.
### Exemples de syntaxe Markdown
**Les titres**
```
   h1 => # ( 1 seul par page / titre de la page )
   h2 => ##
   h3 => ###
```
**Commande**
```
**text** = gras
_text_ = italic
>x = x en tête
```
**Texte**
```
Text
sur plusieurs lignes
```
Code `entre baltique` fin de mon texte


**Liens et Images**
```
 [texte du lien](url_du_lien)

![Texte alternatif](url_de_l'image)
```
