# Valeur primitive 
##'number,string,booleans,undefined,null,NaN';

**caracter d'echappement**
```
'je m\'apelle' =  \ =>caracter d'echappement;
```
**number**
```
'1,2,3,4,0,5' = 'number'
```
**string**
```
'all text'  =  'string';
```
**booleans**
```
'booleans' = 'true,falfe';
```
**undefined**
```
'undefined'= 'something that doesn\'t exist';
```
**null**
```
'null' = 'empty';
```
**NaN**
```
'NaN'='not a number';
```


