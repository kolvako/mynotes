# Ternaire
  ***Cet opérateur est fréquemment utilisé comme raccourci pour la déclaration de if...else***
### On utilise les caractères  `? et :`  pour séparer les 3 opérandes : `condition ? siVrai : siFaux.`
```
const sortedArray = tab
.sort(user1, user2) => user1.firstName < user2.firstName ? -1 : +1;   

```
```
function getFee(isMember) {
  return (isMember ? '$2.00' : '$10.00');
}

console.log(getFee(true));
// expected output: "$2.00"

console.log(getFee(false));
// expected output: "$10.00"

console.log(getFee(1));
// expected output: "$2.00"
```