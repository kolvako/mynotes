# Tableau
## 1) est une structure de données représentant une séquence finie d'éléments auxquels on peut accéder efficacement par leur position, ou indice, dans la séquence.
## 2) les tableaux sont des objets qui servent de liste
```
const tab = [];    => creation

console.log(tab);  => affichage de tableau 

console.table(tab) => affichage de tableau + propre
```
```
index = la position dans un tableau
start counting from   0,  1,  2, ....
console.log(tab[0]);
console.log(tab.length); => longueur de tableau 
console.log(tab[tab.length - 1]); => dernier element 
```



// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri
/*
const sortHotel = hostels.sort((a, b) => b.roomNumbers - a.roomNumbers);
console.table(sortHotel);
*/
// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre
/*
const allRoom = hostels.reduce((acc, hotel) => [...acc, ...hotel.rooms], []).
    filter((room) => room.size >= 3).
    sort((a, b) => a.roomName > b.roomName ? 1 : -1);
console.table(allRoom);
 */
// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places
// nom de chambre a une taille superiore a 15 caractere
/*
const Room = hostels.reduce((acc, hotel) => [...acc, ...hotel.rooms], []).
    filter((room) => room.size > 3).
    filter((room) => room.roomName.length > 15);
console.table(Room);
*/
// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus de 3 places et
// changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres
/*
hostels = hostels.map((hotel) => {
  hotel.rooms = hotel.rooms.filter((room) => room.size < 3);
  hotel.roomNumbers = hotel.rooms.length;
  return hotel;
});
console.table(hostels);
 */
// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)
/*
let ocean = hostels.splice(1, 2);
ocean = hostels.map((hotel) => {
  hotel = hostels[0];
  hotel.rooms.splice(0, 6);
  hotel.roomNumbers = hotel.rooms;
  return hotel;
});
hostels.push(ocean);
console.table(ocean);
*/
// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'
/*
const newhotele = {};
hostels.forEach((hotel) => newhotele[hotel.name] = hotel.rooms.some(
    (room) => room.roomName === 'suite marseillaise'));
console.table(newhotele);
*/

// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom
/*
function getName(id) {
  const hotelToFind = hostels.find((hotel) => hotel.id === id);
  return hotelToFind ? hotelToFind.name : 'pas de hotel';
}

const name = getName(1);
console.table(name);
*/
// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom
// eslint-disable-next-line no-unused-vars
/*
function getRoomName(idHotel, idRoom) {
  const hotelToFind = hostels.find((hotel) => hotel.id === idHotel);
  if (!hotelToFind) {
    return 'pas de hotel';
  }
  const roomToFind = hotelToFind.rooms.find((room) => room.id === idRoom);
  if (roomToFind) {
    return roomToFind.roomName;
  } else {
    return 'pas de room';
  }
}

const result = getRoomName(3, 2);
console.table(result);
*/
// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.
/* function allrooms(hostels) {
  return hostels = maj(name);
}
function maj(name) {
  return name.charAt(0).toUpperCase();
}
console.table(hostels.every(allrooms));
*/
// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou égale à 5 places
/*
function allHotel(hostels) {
  return hostels.filter(
      (hotel) => hotel.rooms.some((hotel) => hotel.size >= 5)).
      map((hotel) => hotel.id);
}

console.table(allHotel(hostels));
*/
// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie toutes les chambres qui ont plus de 3 places, dans un hotel
// avec piscine et qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient
/*
function allHotel(hostels) {
  return hostels.filter((listHot) => listHot.rooms.map((listHot) => listHot.size > 3))
      .map((listHot) => listHot.rooms);
}
console.table(allHotel(hostels));
*/
