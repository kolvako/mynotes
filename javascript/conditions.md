# if  &  else
## Utilisez  l'instruction `if` pour spécifier un bloc de code JavaScript à exécuter si une condition est vraie.
## Utilisez l'instruction `else `pour spécifier un bloc de code à exécuter si la condition est fausse.
```
if (hour < 18) {
  greeting = "Good day";
} else {
  greeting = "Good evening";
}
```