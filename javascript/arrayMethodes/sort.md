# Sort

## 1) trie les éléments d'un tableau, dans ce même tableau, et renvoie le tableau.
## 2) peut également prendre en argument une fonction de rappel (callback) qui détermine la relation d'ordre selon laquelle les éléments sont comparés.
***Pour trier en fonction du plus petit au plus grand***
```
const sortedArray = tab.sort(user1, user2) =>
    return user1.age - user2.age);

console.table(sortedArray);

a<b =>>> -1
a>b =>>>  1
a=b =>>>  0

```
***Pour trier par ordre alphabétique***
```
const sortedArray = tab.sort(user1, user2) => {
    if (user1.firstName < user2.firstName) {
        return -1;
    } else {
        return +1;
    }
});

console.log(sortedArray);
```
## ternaire
   ***Cet opérateur est fréquemment utilisé comme raccourci pour la déclaration de if...else***
### On utilise les caractères  `? et :`  pour séparer les 3 opérandes : `condition ? siVrai : siFaux.`
```
const sortedArray = tab
.sort(user1, user2) => user1.firstName < user2.firstName ? -1 : +1;   

```