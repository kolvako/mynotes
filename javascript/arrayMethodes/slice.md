# Slice
## 1) extrait une portion d'un tableau et renvoie une nouvelle copie du tableau
## 2) tableau original ne sera pas modifie

```
ex;

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

console.log(animals.slice(2));
// expected output: Array ["camel", "duck", "elephant"]

console.log(animals.slice(2, 4));
// expected output: Array ["camel", "duck"]

console.log(animals.slice(1, 5));
// expected output: Array ["bison", "camel", "duck", "elephant"]

```