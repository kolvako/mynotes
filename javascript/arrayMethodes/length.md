# Length
## 1) indique le nombre d'elements presents dans le tableau
## 2) renvoie toujours l'indice du dernier élément plus 1
## 3) on peut assigne une valeur a la propriete lenght
```
ex;

const cars =["Peugeot" , "Volvo" , "Mercedes"];
console.log(cars.length);  =>>>>>  3

 
const cars =["Peugeot" , "Volvo" , "Mercedes"];
cars.length = 2;
console.log(cars.length);  =>>>>>  2
```
