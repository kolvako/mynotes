# Some
## c'est l'inverse l'opérateur every(), s'il y a un seul qui valide le prédicat,l'opérateur renvoie true et s'il y a aucun qui valide le prédicat il renvoie false
```
const allHostelsHavePools = hostels.some(hostel => hostel.pool === true);

console.log(allHostelsHavePools);
```
***La console devrait renvoyer true. Si on avait mis `=== false` il aurait renvoyer true aussi, car dans la liste il y en a au moins un qui est false.***