# Push
##  ajoute un ou plusieurs éléments à la fin d'un tableau et retourne la longueur du tableau après cet ajout  
```
onst animals = ['pigs', 'goats', 'sheep'];

console.log(animals.push('cows'));
// expected output: 4

console.log(animals);
// expected output: Array ["pigs", "goats", "sheep", "cows"]

```

## Pour créer un tableau vide et ajouter des éléments à ce tableau 
```
const tab2 = [];

const car1 = {
    name: 'peugeot'
};

const car2 = {
    name: 'volvo'
}

tab2.push(car1, car2);

console.table(tab2);

```