# map
## 1) La map()méthode crée un nouveau tableau avec les résultats de l'appel d'une fonction pour chaque élément du tableau.
## 2) map renvoie une valeur  ex: const tab2 =

```
const array1 = [1, 4, 9, 16];

const map1 = array1.map(x => x * 2);

console.log(map1);

 [2, 8, 18, 32]
```
***La méthode map créer un nouveau tableau (tab2) à partir du premier (tab)***
```
const tab2 = tab.map((user, index) => {
    user.index = index;
    return user;
});
console.table(tab2);
```
***On a modifié le tableau en lui ajoutant une propriété index. Alors, en plus de créer un nouveau tableau, le tableau de référence (tab) sera modifié aussi. Pour remédier à ce problème il faut écrire le code comme ceci***
```
const tab2 = tab.map((user, index) => {
    const temp = {...user};
    temp.index = index;
    return temp;
});

```
***Pour créer le nouvel objet on rajoute les trois petits points comme dans le code ci-dessus. Cela va copier le tableau de base et c'est cette copie qui sera modifié et non pas le tableau d'origine***
