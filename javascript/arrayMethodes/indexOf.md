# indexOf
##La methode indexOf() renvoie le premier indice pour lequel on trouve un element donne dans un tableau. Si l'element cherche n'est pas present dans le tableau, la methode renverra -1.
```
const beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];

console.log(beasts.indexOf('bison'));
// expected output: 1

// start from index 2
console.log(beasts.indexOf('bison', 2));
// expected output: 4

console.log(beasts.indexOf('giraffe'));
// expected output: -1

```