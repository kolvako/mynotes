# Find
## 1) renvoie la valeur du premier élément trouvé dans le tableau qui respecte la condition donnée par la function de test passée en argument. Sinon, la valeur undefined est renvoyée.
## 2) renvoi un objet et il ne renvoi pas un tableau
## 3) permet de  renvoyer la valeur du premier élement trouvé il prend une function callback qui renvoi un predicat(true ou false)

```
const hotelToFind = hostels.find(hotel => hotel.name === "hotel ocean");

console.log(hotelToFind);
```
***Pour trouver un prédicat***
```
const hotelToFind = hostels.find(hotel => hotel.pool === true);

console.log(hotelToFind);
```
##La console devrait renvoyer le premier élément true seulement et pas les autres true. En effet, le premier true est trouvé et renvoyé, puis la fonction s'arrete.
`Dans tous les cas on ne peut pas enchaîner avec d'autres opérateurs si on utilise un find`