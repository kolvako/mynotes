# Reduce
## C'est une méthode qui utilise un accumulateur qui permet d'accumuler des éléments.
***ex avec number***
```
const tabNumber = [2, 5, 10, 2, 33];

const total = tabNumber.reduce((acc, currentValue) => acc = acc + currentValue, 0);
console.log(total);
// 52
```
***ex avec un objet en utilisant CONCAT***
```

const tabNumber = [
    {firstName: 'toto', age: 12, hobbies: ['foot', 'playstation']},
    {firstName: 'titi', age: 13, hobbies: ['foot', 'playstation']},
];

const total = tabNumber
.reduce((acc, currentValue) => acc.concat(currentValue.firstName.age), []);
console.log(total);
// ["toto", "titi"]

```
***Une autre méthode***
```
const tabNumber = [
    {firstName: 'toto', age: 12, hobbies: ['foot', 'playstation']},
    {firstName: 'titi', age: 13, hobbies: ['foot', 'playstation']},
];

const total = tabNumber.reduce((acc, currentValue) => [...acc, ...currentValue.hobbies], []);

console.log(total);
//  ["foot", "playstation", "foot", "playstation"]
```