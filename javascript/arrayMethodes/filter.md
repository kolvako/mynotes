# filter

## La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction callback.

***filter ne modifie pas le tableau d'origine***

***const et let   faut les declarer avant de le utiliser***
```
ex;

const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
const result = words.filter(word => word.length > 6);

console.log(result);    =>>>> ["exuberant", "destruction", "present"]
```
```
const filArray = tab
    .filter(user =>user.age <30)
    .map (user => {
            user.lastName = user.lastName + 'y';
            return user
        });                      ====>ex1
```
***La programmation fonctionnelle = on fait enchainer des fonctions sur des méthodes***
```
const filArray = tab
    .filter(isUnder30)           =====> ex2 programmation fonctionnelle
    .map (addYAtEnd)                        top de top  :)

function isUnder30(user) {
    return user.age <30
}
function addYAtEnd(user) {
    user.lastName = user.lastName + 'Y';
    return user
}
console.table(filArray);   ====>meme resultat ex1,ex2
```
```
const et let   faut les declarer avant de le utiliser
```