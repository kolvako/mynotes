# Concat
## 1) fusionne un ou plusieurs tableaux et renvoie le résultat de cette fusion
## 2) ne modifie pas les tableau existant
``` 
const tab2=[1, 2, 3]
const tab3=[4, 5, 6]

const tab4 = tab2.concat(tab3)     // mauvaise methode

const tab4 = [...tab2, ...tab3]  //bonne methode
console.table(tab4);

// [1 2 3 4 5 6]
```