# forEach
## 1) itère une variable donnée sur toutes les propriétés d'un objet.
## 2) prend un objet et faire quelque chose avec chacun des objet.
## 3) la méthode n’utilise pas une copie du tableau lorsqu’elle est appelée, elle manipule le tableau directement.
## 4) la méthode forEach exécute la fonction callback

```
const numbers =[ 2, 20; 40, 4];
numbers.forEach(myFunction)

function myFunction(item, index, arr) {
           arr[index] = item *5;
});
   =>>>> [10, 100, 200, 20]
```
```
const array1 = ['a', 'b', 'c'];

array1.forEach(function(element) {
  console.log(element);
});
``` 