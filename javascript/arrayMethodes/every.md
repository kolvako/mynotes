# Every
## La méthode every() permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument.
## Cette méthode renvoie un boolean`true ou false`

```
const isBelowThreshold = (currentValue) => currentValue < 40;

const array1 = [1, 30, 39, 29, 10, 13];

console.log(array1.every(isBelowThreshold));
// expected output: true

``` 
```
const allHostelsHavePools = hostels.every(hostel => hostel.pool === true);

console.log(allHostelsHavePools);

```