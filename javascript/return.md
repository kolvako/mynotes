# Return
##Return met fin à l'exécution d'une fonction et définit une valeur à renvoyer à la fonction appelante.
```
-Return est utilisée dans une fonction, l'exécution de la fonction se termine.
-Si une valeur est fournie, elle sera renvoyée à l'appelant de la fonction.
-Si valeur absente, la valeur _undefined_ sera renvoyée.
-Il est interdit d'avoir un caractère de fin entre le mot _return_ et l'expression.
-On peut, si besoin, utiliser des parenthèses.
```