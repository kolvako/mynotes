# Operaror Math
## L'objet Math JavaScript vous permet d'effectuer des tâches mathématiques sur des nombres.
### Math.round ()
***Renvoie la valeur de x arrondie à son entier le plus proche***
```
Math.round(4.7);    // returns 5
Math.round(4.5);    // returns 5  
Math.round(4.4);    // returns 4
```
### Math.floor ()
***Retourne le plus grand entier inférieur ou égal à la valeur passée en paramètre***
```
Math.floor(4.7);    // returns 4
```
### toFixed()
***Permet de formater un nombre en notation à point-fixe***
```
var numObj = 12345.6789.toFixed()

numObj.toFixed();       // Renvoie '12346' : arrondi, aucune partie fractionnaire
numObj.toFixed(1);      // Renvoie '12345.7' : arrondi ici aussi
numObj.toFixed(6);      // Renvoie '12345.678900' : des zéros sont ajoutés
2.34.toFixed(1);        // Renvoie '2.3'
-2.34.toFixed(1);       // Renvoie -2.3 (en raison de la précédence des opérateurs, 
                        // les littéraux de nombres négatifs ne renvoient pas de chaînes)
2.35.toFixed(1);        // Renvoie '2.4' (arrondi supérieur)
2.55.toFixed(1);        // Renvoie '2.5' (cf. l'avertissement ci-avant)
(-2.34).toFixed(1);     // Renvoie '-2.3'
``` 

### parseFloat ()
***Est utilisée pour accepter la chaîne et la convertir en nombre à virgule flottante***
```
parseFloat ("100") = 100
parseFloat ("2018 @ geeksforgeeks") = 2018
parseFloat ("geeksforgeeks @ 2018") = NaN
parseFloat ("3.14") = 3.14
parseFloat ("22 7 2018") = 22
console.log(parseFloat(string '10.67') + 10);  = 20.67
```
### parseInt ()
***Est utilisée pour accepter la chaîne et la convertir en entier***
```
parseInt ("100") = 100
parseInt ("2018 @ geeksforgeeks") = 2018
parseInt ("geeksforgeeks @ 2018") = NaN
parseInt ("3.14") = 3
parseInt ("21 7 2018") = 21
```
### Math.random()
***Renvoie un nombre flottant pseudo-aléatoire compris entre 0 (inclus) et 1 (exclusif):
```
Math.floor(Math.random() * 10);     // returns a random integer from 0 to 9
Math.floor(Math.random() * 11);      // returns a random integer from 0 to 10
```

















