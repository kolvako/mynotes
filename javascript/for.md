# for
## Crée une boucle composée de trois expressions optionnelles séparées par des points-virgules et encadrées entre des parenthèses, est exécutée tant qu'une condition est vraie.
``` 
for ([initialisation]; [condition]; [expression_finale])
for (let i = 0; i < 10; i = i + 1) {
        console.log('titi'+ 1);
    }
// result:   titi1 =>>> titi9
```
