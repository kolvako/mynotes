# Function
### One function is un programme dans le programme. (une sorte de sous-programme)
## Arrow function vs function
***la différence pour que arrow function marche il faut la décralér.`const or let`***
```
ex  funcion
 
function addATob(a,b){
    return a+b;
}
console.log(addATob(2,6));
Résultat //8
```

```
ex arrow function

const addATob= (a,b) => {
    return a+b;
}
console.log(addATob(2,6));
Résultat //8
```
***exemple arrow function avec {} et sans {}***
```
tab.filter(()=>{
    return a+b
})

tab.filter(()=>a+b)
```
    