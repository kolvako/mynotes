# Git configuration
```
- Se rendre sur Git et l'installer
- Lancer le luncher d'install et faire next

- Ouvrir le terminal de commande
- Saisir son username:
  git config --global user.name "FIRST_NAME LAST_NAME"

- Saisir son adresse email:
  git config --global user.email "MY_NAME@example.com"

- Dans settings, cliquer sur plugin et installer gitlab projects
- Redémarrer Webstorm
- Rertourner dans settings, puis version control et aller sur gitlab
- Saisir https://gitlab.com dans la 1ere ligne
- Saisir gitlab.com dans la 2eme
- Cliquer sur token, puis sur API et créer le token
- Saisir le token
- Git est activé, saisir git init dans le terminal pour s'en assurer

```