# NewBranch
``` 
- Pour que nos modification soient intégrées au master, il faut cliquer en bas sur Git: master

- Créer une nouvelle branche

- On peut alterner entre les deux versions avec checkout

- Il faut commiter avant de checkouter la branche

- Une fois la branche crée, il faut demander l'autorisation pour l'envoyer vers la master

- Il faut aller sur git et cliquer sur create merge request

- Verifier si on va de branch vers master

- On peut s'assigner pour faire la demande au chef

- Enfin pour recuperer la modification on retourne sur webstorm, puis sur la master,
  on clique sur update (fleche bleue) et on clique sur merge.
```
