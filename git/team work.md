# Team work
**Récupérer, travailler et envoyer son travail**
```
- Cliquer sur la fleche bleue pour récuperer la master
- Créer une branche pour apporter des modifications au projet
- Faire les modifications
- Commiter et pusher
- Aller sur gitlab et cliquer sur create merge request
- Choisir le responsable qui va gérer les conflits
- Cliquer sur submit, le projet a été envoyé
```
**Récupération du travail chez le chef de projet**
```
- Reception d'une notification qui prévient le chef de l'arrivé de la request
- Examens du code en cliquant sur changes et validation en cliquant sur merge
- En cas de conflit on clique sur resolve conflict
- Le chef écrit au developpeur et demande de resoudre les conflits
``` 
**Récéption de l'information chez le developpeur**
```
- Se mettre sur master et clicker sur la fleche bleue pour recuperer la dernière version
- Retourner sur sa branche, cliquer sur vcs git et cliquer sur merge changes
- Cliquer sur master et la merger dans la branche
- Resoudre les conflits en cliquant sur le fichier puis sur merge un par un
- Une fenetre apparait : a gauche ma version a moi, a droite la version de joueur 1
- Modifier le code et cliquer sur apply
- Signaler que la merge a été faite en appuyant sur shift + commande + k (racourci push)
- Pusher et ensuite sur Gitlab cliquer sur merge dans la request
``` 